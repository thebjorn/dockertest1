
install
=======
::
   
    choco install minikube
    choco install kubernetes-cli

create a new external virtual switch in hyper-v (name it
"Primary Virtual Switch").

Start minikube VM on hyper-v::

    minikube start --vm-driver hyperv --hyperv-virtual-switch "Primary Virtual Switch"

    minikube stop

start dashboard::
    
    minikube dashboard

start docker image::

    kubectl create deployment hello-node --image=...
    kubectl expose deployment hello-node --type=LoadBalancer --port=8080

note: the port must match the port exposed in the docker image
(this should do something sensible on a public cloud)
To run locally::

    minikube service hello-node

    
Module 3
========
.. image:: imgs/node-pods.jpg

(https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-interactive/)
::

    kubectl get, describe, logs and exec

    kubectl get pods -o go-template --template "{{range .items}} {{.metadata.name}}{{\"\n\"}}{{end}}"

    c:\srv> kubectl get pods -o go-template --template "{{range .items}} {{.metadata.name}}{{\"\n\"}}{{end}}"
    hello-node-6d69b59589-fzqft

    c:\srv> set POD_NAME=hello-node-6d69b59589-fzqft

    http://localhost:8001/api/v1/namespaces/default/pods/hello-node-6d69b59589-fzqft/proxy/


whereami
========
currently starting: https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/